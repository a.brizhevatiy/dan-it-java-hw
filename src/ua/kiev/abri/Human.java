package ua.kiev.abri;

import java.util.concurrent.ThreadLocalRandom;

import static ua.kiev.abri.hw4.myArrayToString;

// --HUMAN--
public class Human {
    {
        System.out.printf("Создается новый объект %s\n", this.getClass().getSimpleName());
    }

    static {
        System.out.printf("Загружается класс %s\n", "Human");
    }

    private int year;
    private int iq;
    private String name;
    private String surname;
    private String[][] scheldule;
    private Family family;

    //        private Pet myPet;  //----------> moved to Family
    //        private Human mother;//----------> moved to Family
    //        private Human father;//----------> moved to Family

    // Human constructors --------------------->
    public Human() {
    }

    public Human(String name, String surname, int year) {
        this.year = year;
        this.name = name;
        this.surname = surname;
    }
// path of this moved to Family, so it's duplicate previous constructor after that.
//        public Human(String name, String surname, Human mother, Human father) {
//            this.year = year;
//            this.name = name;
//            this.surname = surname;
//            this.mother = mother;
//            this.father = father;
//        }

    //        public Human(String name, String surname, int year, int iq, String[][] scheldule, Pet myPet, Human mother, Human father) {
    public Human(String name, String surname, int year, int iq, String[][] scheldule) {
        this.year = year;
        this.iq = iq;
        this.name = name;
        this.surname = surname;
        this.scheldule = scheldule;
//            this.myPet = myPet;//----------> moved to Family
//            this.mother = mother;//----------> moved to Family
//            this.father = father;//----------> moved to Family
    }

    //<----------------------------------------

    // Human Getters and Setters ------------->
    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String[][] getScheldule() {
        return scheldule;
    }

    public void setScheldule(String[][] scheldule) {
        this.scheldule = scheldule;
    }

//----------> moved to Family
//        public Pet getMyPet() {
//            return myPet;
//        }
//
//        public void setMyPet(Pet myPet) {
//            this.myPet = myPet;
//        }
//
//        public Human getMother() {
//            return mother;
//        }
//
//        public void setMother(Human mother) {
//            this.mother = mother;
//        }
//
//        public Human getFather() {
//            return father;
//        }
//
//        public void setFather(Human father) {
//            this.father = father;
//        }
    //<----------------------------------------

//        public void greetPet() {
//            System.out.printf("Привет, %s!\n", myPet.nickname);
//        }
//
//        public void describePet() {
//            String trickLevel = myPet.trickLevel > 50 ? "очень хитрый" : "почти не хитрый";
//            System.out.printf("У меня есть %s, ему %d лет, он %s.\n", myPet.species, myPet.age, trickLevel);
//        }

    public boolean feedPet(boolean timeToIt) {
        if (timeToIt) {
            int rand = ThreadLocalRandom.current().nextInt(0, 101);
            System.out.printf("%d --> число для определения кормления\n", rand);
            System.out.println(
                    rand > this.family.getPet().getTrickLevel()
                            ? String.format("Думаю, %s не голоден.", this.family.getPet().getNickname())
                            : String.format("Хм... покормлю ка я %s.", this.family.getPet().getNickname()));
            return true;
        } else {
            System.out.printf("Думаю, %s не голоден.", this.family.getPet().getNickname());
            return false;
        }
    }

    @Override
    public String toString() {
        return String.format("Human{name=%s, surname=%s, year=%d, iq=%d, scheldule=%s}",
                this.name,
                this.surname,
                this.year,
                this.iq,
                myArrayToString(this.scheldule));
    }
//        public String toString() {
////            return String.format("Human{name=%s, surname=%s, year=%d, iq=%d, mother=%s %s, father=%s %s, pet=%s}\n",
//            return String.format("Human{name=%s, surname=%s, year=%d, iq=%d}\n",
//                    this.name,
//                    this.surname,
//                    this.year,
//                    this.iq);
//        }
}

