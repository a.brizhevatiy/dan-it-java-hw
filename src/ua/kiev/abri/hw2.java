package ua.kiev.abri;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;
import java.util.StringJoiner;

public class hw2 {

    public static int randomNumber(int min, int max) {
        Random random = new Random();
        int dif = max - min;
        return random.nextInt(dif + 1) + min;
    }// to get the random number

    public static void printArr(String[][] arr) {
        for (String[] s : arr) {
            System.out.printf("%s\n", myArrayToString(s));
        }
    } // Print arrays

    public static String myArrayToString(String[] a) {
        StringJoiner sj = new StringJoiner("|");
        for (String x : a) {
            sj.add(String.format("%3s", x));
        }
        return sj.toString() + "|";
    } // formatting field output

    public static String[][] createGameField(int size) {
        int newSize = size + 1;
        int xCount = 1;
        String[][] field = new String[newSize][newSize];
        for (int i = 0; i < newSize; i++) {
            for (int j = 0; j < newSize; j++) {
                if (j == 0) {
                    field[i][j] = i + " ";
                } else if (i == 0) {
                    field[i][j] = xCount + " ";
                    xCount += 1;
                } else {
                    field[i][j] = " - ";
                }
            }
        }
        return field;
    } // creating array for game

    public static boolean isInteger(String s) {
        return isInteger(s, 10);
    } // check for int

    public static boolean isInteger(String s, int radix) {
        if (s.isEmpty()) return false;
        for (int i = 0; i < s.length(); i++) {
            if (i == 0 && s.charAt(i) == '-') {
                if (s.length() == 1) return false;
                else continue;
            }
            if (Character.digit(s.charAt(i), radix) < 0) return false;
        }
        return true;
    } // check for int

    public static int usersNumberInput(int from, int to) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String num = reader.readLine().trim();
        String error = String.format("Try to enter the correct number from %d to %d:\n", from, to);

        while (!isInteger(num) || (from > Integer.parseInt(num)) || (Integer.parseInt(num) > to)) {
            System.out.printf("%s", error);
            num = reader.readLine();
        }
        return Integer.parseInt(num);
    }// check for the correct input

    public static int[][] compareArr(int[] arr1, int[] arr2) {
        int[][] array = new int[arr2.length][2];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < arr2.length; j++) {
                array[j][i] = i == 0 ? arr1[j] : arr2[j];
            }
        }

        return array;
    } //making one two-dimensions array from two arrays

    public static int[] shipDeploy(String[][] gameField) {
        int[] shipCell = new int[2];
        shipCell[0] = randomNumber(1, gameField.length - 1);
        shipCell[1] = randomNumber(1, gameField.length - 1);
        System.out.println("For test:\n" + Arrays.toString(shipCell));// !!!!!!!!!!!!!!!!!!!!! FOR TEST ONLY !!!!!!!!!!!!!!!!!!!!!!!!
        return shipCell;
    } //generating ship coordinates // simple path

    public static int[][] bigShipDeploy(int fieldsLimit) {
        int direction = randomNumber(0, 1);
        int[][] shipCells;
        int[] arr1 = new int[3];
        int[] arr2 = new int[3];
        arr1[0] = randomNumber(1, fieldsLimit);
        arr2[0] = arr2[1] = arr2[2] = randomNumber(1, fieldsLimit);

        if (arr1[0] + 2 > fieldsLimit) {
            if (arr1[0] + 1 > fieldsLimit) {
                arr1[1] = arr1[0] - 1;
                arr1[2] = arr1[0] - 2;
            } else {
                arr1[1] = arr1[0] + 1;
                arr1[2] = arr1[0] - 1;
            }
        } else {
            arr1[1] = arr1[0] + 1;
            arr1[2] = arr1[0] + 2;
        }

        if (direction == 0) {
            shipCells = compareArr(arr1, arr2);
        } else {
            shipCells = compareArr(arr2, arr1);
        }

        for (int[] coords : shipCells) {
            System.out.println("For test:\n" + Arrays.toString(coords));// !!!!!!!!!!!!!!!!!!!!! FOR TEST ONLY !!!!!!!!!!!!!!!!!!!!!!!!
        }
        return shipCells;
    } //generating ship coordinates

    public static boolean shoot(int x, int y, int[] shipCell) {
        return y == shipCell[0] && x == shipCell[1];
    } //shooting // simple path

    public static boolean shoot(int x, int y, int[][] shipCell) {
        boolean res = false;
        for (int[] a : shipCell) {
            if (a[0] == y && a[1] == x) {
                res = true;
                break;
            }
        }
        return res;
    } //shooting

    public static void miss(int x, int y, String[][] gameField) {
        String missedMessage = "Your missed!\n";
        String missIcon = " * ";
        gameField[y][x] = missIcon;
        System.out.printf("%s", missedMessage);
    } //ups...

    //    public static void game(String[][] gameField, int[] shipCell) throws IOException { // simple path
    public static void game(String[][] gameField, int[][] shipCell) throws IOException {
        int min = 1;
        int max = 5;
//        int shipSize = 1;
        int shipSize = 3;
        String startText = "All set. Get ready to rumble!\n";
        String endText = "You have won!\n";
        String hitText = "Your hit it! Nice!\n";
        String enterLine = String.format("Please, enter the line number from %d to %d for shot:\n", min, max);
        String enterColumn = String.format("Please, enter the column number from %d to %d for shot:\n", min, max);
        String shootAgain = "The next shot!\n";
        int lineNumber;
        int columnNumber;

        System.out.printf("%s", startText);

        do {
            System.out.printf("%s", enterLine);
            lineNumber = usersNumberInput(min, max);
            System.out.printf("%s", enterColumn);
            columnNumber = usersNumberInput(min, max);
            if (shoot(columnNumber, lineNumber, shipCell)) {
                if (!gameField[lineNumber][columnNumber].equals(" X ")) {
                    shipSize -= 1;
                }
                gameField[lineNumber][columnNumber] = " X ";
                if (shipSize == 0) {
                    System.out.printf("%s", endText);
                    printArr(gameField);
                    break;
                }
                System.out.printf("%s", hitText);
                printArr(gameField);
                continue;
            }

            miss(columnNumber, lineNumber, gameField);
            printArr(gameField);
            System.out.printf("%s", shootAgain);
        } while (true);
    } // Let the game begin!

    public static void main(String[] args) throws IOException {
        int size = 5;
        String[][] gameField = createGameField(size);
//        int[] shipCell = shipDeploy(gameField); // simple path
        int[][] bigShipCells = bigShipDeploy(size);
        game(gameField, bigShipCells);
    }
}
