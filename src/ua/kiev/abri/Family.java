package ua.kiev.abri;

import java.util.Arrays;
import java.util.Objects;

// --FAMILY--
public class Family {
    {
        System.out.printf("Создается новый объект %s\n", this.getClass().getSimpleName());
    }

    static {
        System.out.printf("Загружается класс %s\n", "Family");
    }

    private Human mother;
    private Human father;
    private Human[] children = new Human[0];
    private Pet pet;

    // Family constructor --------------------->
    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.mother.setFamily(this);
        this.father.setFamily(this);
    }
    //<----------------------------------------

    public void addChild(Human child) {
        int length = this.children.length;
        this.children = Arrays.copyOf(this.children, length + 1);
        this.children[length] = child;
        child.setFamily(this);
    }

    public boolean deleteChild(int id) {
        if (this.children.length == 0 || id >= this.children.length) return false;
        Human[] tmp = new Human[this.children.length - 1];
        int tmpCount = 0;
        for (int i = 0; i < this.children.length; i++) {
            if (id != i) {
                tmp[tmpCount++] = this.children[i];
            } else {
                this.children[i].setFamily(null);
            }
        }
        this.children = tmp;
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;
        Family family = (Family) o;
        return mother.equals(family.mother) && father.equals(family.father) && Arrays.equals(children, family.children) && pet.equals(family.pet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    public boolean deleteChild(Human child) {
        for (int i = 0; i < this.children.length; i++) {
            if (this.children[i].equals(child)) {
                return deleteChild(i);
            }
        }
        return false;
    }

    public void greetPet() {
        System.out.printf("Привет, %s!\n", pet.getNickname());
    }

    public void describePet() {
        String trickLevel = pet.getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый";
        System.out.printf("У меня есть %s, ему %d лет, он %s.\n", pet.getSpecies(), pet.getAge(), trickLevel);
    }

    public void feedPet() {
        System.out.printf("My %s %s is eating!\n", pet.getSpecies(), pet.getNickname());
    }

    public int countFamily() {
        return 2 + this.children.length;
    }

    // Family Getters ang Setters ---------------------->
    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    //<----------------------------------------

    @Override
    public String toString() {

        StringBuilder children = new StringBuilder("no childrens.");
        if (this.children.length > 0) {
            children = new StringBuilder();
            for (Human c : this.children) {
                children.append(c.toString());
            }
        }
        return String.format("Mother: %s, father: %s, children: %s, pet: %s",
                this.mother.toString(),
                this.father.toString(),
                children.toString(),
                pet.toString());
    }
}
