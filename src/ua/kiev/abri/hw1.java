package ua.kiev.abri;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;

public class hw1 {

    public static int randomNumber(int min, int max) {
        Random random = new Random();
        int dif = max - min;
        return random.nextInt(dif + 1) + min;
    } // to get the random number

    public static void myPrint(String toPrint) {
        System.out.println(toPrint);
    } // for fun

    public static boolean guessTheNumber(int number, int resultNumber) {
        String smaller = "Your number is too small. Please, try again.\n";
        String bigger = "Your number is too big. Please, try again.\n";
        if (number < resultNumber) {
            myPrint(smaller);
            return false;
        } else if (number > resultNumber) {
            myPrint(bigger);
            return false;
        } else {
            return true;
        }
    } // checking the number from user for bigger or smaller

    public static boolean isInteger(String s) {
        return isInteger(s, 10);
    } // check for int

    public static boolean isInteger(String s, int radix) {
        if (s.isEmpty()) return false;
        for (int i = 0; i < s.length(); i++) {
            if (i == 0 && s.charAt(i) == '-') {
                if (s.length() == 1) return false;
                else continue;
            }
            if (Character.digit(s.charAt(i), radix) < 0) return false;
        }
        return true;
    } // check for int

    public static int usersNumberInput() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String num = reader.readLine().trim();
        String error = "Try to enter the correct number:\n";

        while (!isInteger(num)) {
            System.out.printf("%s", error);
            num = reader.readLine();
        }
        return Integer.parseInt(num);
    }// check for the correct input

    public static int[] intArrSort(int[] arr) {
        int n = arr.length;
        int tmp = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (arr[j - 1] < arr[j]) {
                    tmp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = tmp;
                }
            }
        }
        return arr;
    } // for array sort from greater to lower

    public static String[][] compareArr(int[] arr1, String[] arr2) {
        String[][] array = new String[2][arr2.length];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < arr2.length; j++) {
                array[i][j] = i == 0 ? arr1[j] + "" : arr2[j];
            }
        }

        return array;
    } //making one two-dimensions array from two arrays

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); // for users inputs

        int[] dates = new int[]{476, 1204, 1445, 1453, 1492, 1569, 1572, 1588, 1773, 1914, 1919, 1936, 1939, 1941, 1944, 1945, 1949, 1961, 1962, 1990};
        String[] events = new String[]{
                "Падение Западной Римской империи",
                "Захват крестоносцами Константинополя",
                "Изобретение книгопечатания И. Гуттенбергом",
                "Гибель Византийской империи",
                "Открытие Нового Света Х. Колумбом",
                "Образование Речи Посполитой",
                "Варфоломеевская ночь",
                "Разгром испанской Непобедимой армады",
                "\"Бостонское чаепитие\"",
                "Начало Первой мировой (Великой) войны",
                "Учреждение Лиги Наций",
                "Оккупация Германией Рейнской демилитаризованной зоны",
                "Начало Второй мировой войны",
                "Японская атака на Пёрл-Харбор",
                "Высадка в Нормандии",
                "Атомная бомбардировка Хиросимы и Нагасаки",
                "Образование НАТО",
                "Возведение Берлинской стены",
                "Карибский кризис",
                "Объединение Германии"
        };

        String[][] gameArray = compareArr(dates, events);

        int min = 0;
        int max = gameArray.length-1;

        String askName = "Please, enter your name: ";
        myPrint(askName);
        String name = reader.readLine().trim();

//        String rule = "Please, enter number from 0 to 100: "; // for the simple path.
        String rule = "Please, enter year of this event:\n";
        String ltgb = "Let the game begin!";
        String win = String.format("Congratulations, %s!", name);

        int[] numbers = new int[10]; // for the all user numbers, include the win number
        int numbersCount = 0;

        myPrint(ltgb);
        int number = randomNumber(min, max);

        while (true) {
            myPrint(rule + gameArray[1][number]);
            int userNumber = usersNumberInput();
            numbers[numbersCount] = userNumber;
            numbersCount += 1;
//            if (guessTheNumber(userNumber, number))) { // for the simple path.
            if (guessTheNumber(userNumber, Integer.parseInt(gameArray[0][number]))) {
                myPrint(win);
                break;
            }
            if (numbersCount + 1 == numbers.length) {
                numbers = Arrays.copyOf(numbers, numbers.length + 5);
            }
        }
        numbers = Arrays.copyOfRange(numbers, 0, numbersCount);
        myPrint(String.format("Your numbers:\n %s", Arrays.toString(intArrSort(numbers))));
    }
}