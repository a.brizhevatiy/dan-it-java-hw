package ua.kiev.abri;

import java.util.Arrays;

// --PET--
public class Pet {
    {
        System.out.printf("Создается новый объект %s\n", this.getClass().getSimpleName());
    }

    static {
        System.out.printf("Загружается класс %s\n", "Pet");
    }

    private int age;
    private int trickLevel;
    private String species;
    private String nickname;
    private String[] habits;

    // Pet constructors --------------------->
    public Pet() {
    }

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.age = age;
        this.trickLevel = trickLevel;
        this.species = species;
        this.nickname = nickname;
        this.habits = habits;
    }
    // <--------------------------------------

    // Pet Getters and Setters -------------->
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }
    //<------------------------------------------

    public void eat() {
        System.out.println("Я кушаю!");
    }

    public void respond() {
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!\n", this.nickname);
    }

    public void foul() {
        System.out.println("Нужно замести следы.....");
    }

    @Override
    public String toString() {
        return String.format("%s{nickname=%s, age=%d, trickLevel=%d, habits=%s}\n",
                this.species,
                this.nickname,
                this.age,
                this.trickLevel,
                Arrays.toString(this.habits));
    }
}

