package ua.kiev.abri;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringJoiner;

public class hw3 {
    public static void printArr(String[][] arr) {
        for (String[] s : arr) {
            System.out.printf("%s\n", myArrayToString(s));
        }
    } // Print arrays

    public static String myArrayToString(String[] a) {
        StringJoiner sj = new StringJoiner(": ");
        for (String x : a) {
            sj.add(String.format("%s", x));
        }
        return sj.toString();
    } // formatting field output

    public static String[][] createArr() {
        String[][] scedule = new String[7][2];
        String week = "Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday";
        String[] tasks = new String[]{"do home work", "go to courses; watch a film", "go to work", "go to GYM", "walk in forest", "drink", "sleep till launch"};
        for (int i = 0; i < scedule.length; i++) {
            scedule[i][0] = week.split(",")[i].trim();
            scedule[i][1] = tasks[i];
        }
        return scedule;
    }

    public static void changeTask(String day, String[][] arr, BufferedReader reader) throws IOException {
        String newTaskText = String.format("Please, input new tasks for %s:\n", day);

        System.out.printf("%s", newTaskText);
        String newTask = reader.readLine().trim();

        for (int i = 0; i < arr.length; i++) {
            if (arr[i][0].equalsIgnoreCase(day)) {
                arr[i][1] = newTask;
                System.out.printf("Your tasks for %s: %s.\n", arr[i][0], arr[i][1]);
                break;
            }
        }
    }

    public static void changeDay(String oldDay, String[][] arr, BufferedReader reader) throws IOException {
        String newDayText = "Please, input the new day for this tasks:\n";
        String tmp = "";
        String errorMsg = "The day is not correct!\n";
        int oldDayIndex = -1;
        boolean error = false;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i][0].equalsIgnoreCase(oldDay)) {
                tmp = arr[i][1];
                arr[i][1] = "No tasks";
                oldDayIndex = i;
                break;
            }
        }
        if (tmp.equals("")) {
            System.out.printf("%s", errorMsg);
            return;
        }

        System.out.printf("%s", newDayText);
        String newDay = reader.readLine().trim();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i][0].equalsIgnoreCase(newDay)) {
                arr[i][1] = tmp;
                System.out.printf("Your tasks for %s: %s.\n", arr[i][0], arr[i][1]);
                return;
            }
        }
        arr[oldDayIndex][1] = tmp;
        System.out.printf("%s", errorMsg);
    }

    public static void dialogWithUser(String[][] arr) throws IOException {
        String inputText = "Please, input the day of the week, All to see all tasks and Exit for exit:\n";
        String textForExit = "exit";
        String noDayError = "The day of week is needed.\n";
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String usersInput = "";

        while (true) {
            System.out.printf("%s", inputText);
            usersInput = reader.readLine().trim().toLowerCase();
            if (usersInput.equals(textForExit)) {
                System.out.println("Bye!");
                break;
            }
            if (usersInput.equals("all")) {
                printArr(arr);
                continue;
            }
            if (usersInput.contains("change")) {
                String[] data = usersInput.split(" ");
                if (data.length > 1) {
                    String newDay = data[1].trim();
                    changeTask(newDay, arr, reader);
                } else {
                    System.out.printf("%s", noDayError);
                }
                continue;
            }
            if (usersInput.contains("reschedule")) {
                String[] data = usersInput.split(" ");
                if (data.length > 1) {
                    String oldDay = data[1].trim();
                    changeDay(oldDay, arr, reader);
                } else {
                    System.out.printf("%s", noDayError);
                }
                continue;
            }
            String tasks = switchOutput(usersInput, arr);
            System.out.printf("%s", tasks);
        }
    }

    public static String switchOutput(String day, String[][] arr) {
        String tasks = "";
        String errorMsg = "Sorry, I don't understand you, please try again.\n";
        switch (day) {
            case "sunday":
                tasks = arr[0][1];
                break;
            case "monday":
                tasks = arr[1][1];
                break;
            case "tuesday":
                tasks = arr[2][1];
                break;
            case "wednesday":
                tasks = arr[3][1];
                break;
            case "thursday":
                tasks = arr[4][1];
                break;
            case "friday":
                tasks = arr[5][1];
                break;
            case "saturday":
                tasks = arr[6][1];
                break;
            default:
                return errorMsg;
        }
        return String.format("Your tasks for %s: %s\n", day, tasks);
    }

    public static void main(String[] args) throws IOException {
        String[][] scedule = createArr();
        dialogWithUser(scedule);
    }
}
