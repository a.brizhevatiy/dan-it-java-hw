package ua.kiev.abri;

import java.util.Arrays;

public class hw4 {

    public static String myArrayToString(String[][] arr) { // For Arrays toString output
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        if (arr != null) {
            for (String[] a : arr) {
                sb.append(Arrays.toString(a));
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public static void main(String[] args) {
//        Human mother = new Human();
//        Human father = new Human();
//        Human child = new Human();
//        Pet dog = new Pet();
//        Human mother = new Human("Jane", "Karleone", 1960);
//        Human father = new Human("Vito", "Karleone", 1955);
//        Human child = new Human("Michael", "Karleone", 1975);
//        Pet dog = new Pet("dog", "Rock");

        String[][] sceld1 = {{"day", "task"}, {"day1", "task1"}};
        String[][] sceld2 = {{"day2", "task2"}, {"day3", "task3"}};
        String[][] sceld3 = {{"day4", "task4"}, {"day5", "task5"}};
        String[] habits = {"eat", "drink", "sleep"};

        Human mother = new Human("Jane", "Karleone", 1960, 50, sceld1);
        Human father = new Human("Vito", "Karleone", 1955, 55, sceld2);
        Human child = new Human("Michael", "Karleone", 1975, 60, sceld3);
        Human child2 = new Human("Peter", "Blood", 1980);
        Pet dog = new Pet("dog", "Rock", 5, 55, habits);

        Family family = new Family(mother, father);
        family.addChild(child);
        family.setPet(dog);

        System.out.println(mother.toString());
        System.out.println(father.toString());
        System.out.println(child.toString());
        System.out.println(dog.toString());
        System.out.println(family.toString());

        dog.respond();
        dog.eat();
        dog.foul();

        family.greetPet();
        family.describePet();

        System.out.println(child2.toString());
        family.addChild(child2);
        System.out.println(family.toString());
        System.out.println(child2.getFamily().toString());
        family.deleteChild(1);
//        System.out.println(child2.getFamily().toString());  //null pointer exception
        System.out.println(family.toString());
        child.getFamily().greetPet();
        child.feedPet(true);
        family.addChild(child2);
        System.out.println(family.toString());
        family.deleteChild(child2);
        System.out.println(family.toString());

    }
}
